#ifndef CABALLOSYS_H_INCLUDED
#define CABALLOSYS_H_INCLUDED
#include "../Entidades/caballo.h"
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <conio.h>
#include <stdbool.h>



void altaCaballoSys( struct sCaballo datos, char msg[50] );
int cargaListaCaballosSys( struct sListaCaballos* Caballos, char msgError[] , char nombreBinario[50]);
void generaInformeCaballos( char mensaje[]);
bool guardaListaCaballosSys( struct sListaCaballos Caballos, char msg[] , char nombreBinario[50]);

#endif // CABALLOSYS_H_INCLUDED

