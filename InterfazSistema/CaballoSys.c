
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "../AccesoDatos/CaballoAD.h"
#include <stdbool.h>
#include "../InterfazUsuario/interfazGrafica.h"
#include "../Entidades/caballo.h"

bool altaCaballoSys( struct sCaballo vector, char msg[50] )
{

    /*Esta funcion recibe una lista de caballos . Llama a la funcion altaCaballoAD y la envia la cadena que habia sido recibida. Si todo va bien devuelve
    true , si no , false*/

    int n;

    n = altaCaballoAD( vector, "Error " );

    if ( n == 0 )
    {

        return true;
    }
    else
    {

        return false;
    }
}
int cargaListaCaballosSys( struct sListaCaballos*Caballos, char msgError[] , char nombreBinario[50])
{


    /*Esta funcion llama a la funcion cargaListaCaballosAD y comprueba que se haya ejecutado correctamente.Si todo ha ido bien devuelve 1 , si no -1-
    Envia a la funcion cargaListaCaballosAD() la direccion de la lista que ha recibido como parametro y tambien env�a el nombre del fichero que ha recibido como
     par�metro */


    int n = cargaListaCaballosAD( Caballos , nombreBinario);



    if ( n == 1)
    {

        return 1;

    }
    else
    {

        muestraMensajeInfo( msgError);

        return -1;

    }





}

bool generaInformeCaballos( char mensaje[])
{

    /* Esta funcion declara una estructura de tipo sCaballo y abre tanto caballos.dat como infoCaballos.txt . Comprueba que ambos archivos se han abierto correctamente.
    Coge los datos de caballos.dat a traves de la funcion fread y las almacena en la estructura declarada. Luego escribe los datos recibidos en infoCaballos
    a traves de la funcion escribeCaballoTXT*/

    int n;

    struct sCaballo Caballos[50];



    FILE*ptr;
    FILE* ptr2;

    ptr = fopen( "BaseDatos/caballos.dat", "rb" );


    if ( ptr == NULL)
    {

        muestraMensajeInfo( mensaje );
        return false;

    }

    ptr2 = fopen( "BaseDatos/infoCaballos.txt", "wt");



    if ( ptr2 == NULL)
    {

        muestraMensajeInfo( mensaje );
        return false;

    }

    fprintf( ptr2, "Nombre   Die   Victorias    Ganancias\n" );
    fprintf( ptr2, "--------------------------------------------------------------\n" );

    for(n = 0; fread( &Caballos[n], sizeof(struct sCaballo), 1, ptr) == 1; n++)
    {


        escribeCaballoTXT( Caballos[n], ptr2 );
    }


    fclose(ptr);
    fclose(ptr2);

    return true;

}

bool guardaListaCaballosSys( struct sListaCaballos Caballos,  char msg[50] , char nombreBinario[50] )
{

    /*Esta funcion recibe una lista de caballos como parametro y tambien el nombre del archivo donde se quieren guardar los caballos. Llama a la funcion guardaListaCaballosDAT enviandole la lista de caballos recibida
    y tambien enviandole el nombre del archivo recibido . Si todo ha ido bien devuelve true , si no , false*/
    bool n;


    n = guardaListaCaballosDAT( Caballos , nombreBinario );

    if ( n == 0)
    {

        n = true;

    }
    else if ( n == -1)
    {

        sprintf( msg, "No se puede abrir el archivo caballos.txt " );

        n = false;

    }

    return n;

}






