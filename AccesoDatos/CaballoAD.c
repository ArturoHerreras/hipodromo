
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <stdbool.h>
#include "../Entidades/caballo.h"
#include "CaballoAD.h"

void escribeCaballoTXT( struct sCaballo Caballos, FILE*ptr)
{

    /*Esta funcion recibe la lista de caballos y escribe cada miembro en el fichero que se le ha sido transmitido como parametro*/

    fprintf( ptr, "%ld %d %.2f %s\n", Caballos.die, Caballos.victorias, Caballos.ganancias, Caballos.nombre );

}

int cargaListaCaballosAD( struct sListaCaballos* Caballos, char nombreBinario[50])
{

    /*Esta funcion recibe la direccion donde de la lista de caballos donde tiene que almacenar los datos. Tambien recibe el nombre del fichero donde se debe leer los datos,
    y le a�ade "BaseDatos/" para que encuentre correctamente dicho archivo.
    Comprueba que se ha abierto correctamente y utiliza la funcion fread para cargar los datos de los caballos en la direccion obtenida como parametro.
    Utiliza la funcion getNumeroCaballos para cargar el numero de caballos , y utilizando la funcion malloc , establece de forma din�mica el n�mero de caballos que tiene
    que almacenar Caballos.Caballos. Devuelve 1 si todo ha ido correctamente y -1 si ha habido alg�n problema*/



    int i = 0;
    FILE*ptr;
    char fichero[50] = "BaseDatos/";
    strcat( fichero, nombreBinario);
    ptr = fopen( fichero, "rb" );
    char mensaje[100];
    if ( ptr == NULL)
    {

        sprintf( mensaje, "No se puede abrir el archivo caballos.dat " );

        return -1;

    }

    int n = getNumeroCaballos( fichero );


    Caballos->Caballos = malloc( n * sizeof(struct sCaballo));

    if ( Caballos->Caballos == NULL ){

        printf ( "Error en la asignacion de memoria.");
        exit(1);

    }

    while ( i < n ){

        fread( &Caballos->Caballos[i], sizeof(struct sCaballo), 1, ptr );
        i++;
    }

    Caballos->numero_caballos = n;

    fclose ( ptr);
    return 1;


}

int altaCaballoAD(  struct sCaballo datos, char msg[50] )
{

    /*Esta funcion recibe una estructura de caballos de tipo sCaballo . Abre el archivo caballos.dat en modo binario y a�ade los datos recibidos
    en el archivo a traves de la funcion fwrite. Devuelve 0 si todo ha ido bien , -1 si algo ha fallado*/

    FILE*ptr;
    ptr = fopen ( "BaseDatos/caballos.dat", "ab" );

    char mensaje[100];

    if ( ptr == NULL)
    {

        sprintf( mensaje, "No se puede abrir el archivo caballos.dat " );
        return -1;

    }
    else
    {

        fwrite( &datos, sizeof(struct sCaballo), 1, ptr);


        fclose(ptr);
        return 0;
    }
}


bool guardaListaCaballosDAT( struct sListaCaballos Caballos, char nombreBinario[50] )
{

    /*Esta funcion recibe una lista de caballos . Tambien recibe el nombre del archivo donde debe guardar los nuevos datos de caballos. Abre el archivo recibido en modo binario pero lo REESCRIBE por completo. Copia la lista de cababallos recibida
    en el archivo a traves de la funcion fwrite . Devuelve true si todo ha ido bien y false si algo ha fallado*/

    FILE*ptr;
    int n;

    char fichero[20] = "BaseDatos/";

    strcat( fichero, nombreBinario );

    ptr = fopen( fichero, "wb" );

    if ( ptr == NULL)
    {

        return false ;

    }
    for( n = 0 ; n<= ( Caballos.numero_caballos)-1 ; n++)
    {

        fwrite( &Caballos.Caballos[n], sizeof(struct sCaballo), 1, ptr );

    }

    fclose(ptr);
    return true;


}

int getNumeroCaballos( char nombreBinario[50]  )
{

    /*Esta funcion recibe el nombre del archivo donde estan los caballos. Lo abre en modo lectura binaria y utilizando las funciones fseek y ftell , determina el numero de caballos ya que
    ser� el numero de bytes contados por ftell dividido por los bytes que ocupa cada caballos . Devuelve el numero de caballos*/

    FILE*ptr ;

    ptr = fopen( nombreBinario , "rb");
    if ( ptr == NULL)
    {

        return -1;

    }
    fseek( ptr, 0, SEEK_END);
    ftell( ptr );

    int num_caballos;

    num_caballos = ftell(ptr)/sizeof( struct sCaballo);

    fclose(ptr);

    return num_caballos;
}



