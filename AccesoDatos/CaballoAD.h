#ifndef CABALLOAD_H_INCLUDED
#define CABALLOAD_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <stdbool.h>
#include "../Entidades/caballo.h"

void escribeCaballoTXT( struct sCaballo Caballos, FILE* ptr);

int altaCaballoAD( struct sCaballo datos, char msg[50]);

int cargaListaCaballosAD( struct sListaCaballos *Caballos , char nombreBinario[50]);

bool guardaListaCaballosDAT( struct sListaCaballos Caballos , char nombreBinario[50]);

int getNumeroCaballos( char nombreBinario[50] );
#endif // CABALLOAD_H_INCLUDED
