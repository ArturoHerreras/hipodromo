#ifndef INTERFAZGRAFICA_H_INCLUDED
#define INTERFAZGRAFICA_H_INCLUDED

#include "../Entidades/caballo.h"
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

void muestraMensajeInfo( char *msg );
void setColorTexto(WORD colors);

void inicInterfazUsuario();

void gotoxy(int x, int y);

int leerOpcionValida();

void limpiar();

void limpiarMensaje();

#endif // INTERFAZGRAFICA_H_INCLUDED






