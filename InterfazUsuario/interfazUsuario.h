#ifndef INTERFAZUSUARIO_H_INCLUDED
#define INTERFAZUSUARIO_H_INCLUDED
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

void inicInterfazUsuario();

int menuPrincipal(void);

void gestionMenuPrincipal(void);

void pararPrograma();

#endif // INTERFAZUSUARIO_H_INCLUDED
