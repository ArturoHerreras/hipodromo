

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "../Entidades/caballo.h"
#include "interfazGrafica.h"
#include "../InterfazSistema/CaballoSys.h"
#include "caballoIU.h"
#include "interfazUsuario.h"




void altaCaballoIU()
{

    /*Esta funcion declara una estructura de tipo sCaballo . Pide los datos al usuario del caballo a dar de alta y los almacena en dicha estructura
    Llama a la funcion altaCaballoSys y la envia la estructura rellenada con los datos introducidos por el usuario*/

    struct sCaballo vector;


    muestraMensajeInfo("Nombre : " );
    scanf( "%s", vector.nombre );
    gotoxy( 2, 13),
            printf( "Nombre : %s", vector.nombre);
    limpiarMensaje();

    muestraMensajeInfo("Victorias : " );
    scanf("%d", &vector.victorias);
    gotoxy( 2, 15 );
    printf( "Victorias : %d", vector.victorias );
    limpiarMensaje();

    muestraMensajeInfo( "Ganancias : " );
    scanf( "%f", &vector.ganancias );
    gotoxy( 2, 17);
    printf( "Ganancias : %f", vector.ganancias );
    limpiarMensaje();

    muestraMensajeInfo( "DIE : " );
    scanf( "%ld", &vector.die );
    gotoxy( 2, 19);
    printf( "DIE : %ld", vector.die );
    limpiarMensaje();

    altaCaballoSys( vector, "Error" );

}

void muestraCaballo( struct sCaballo Caballos )
{

    /*Esta funcion recibe una estructura de tipo sCaballo y escribe sus miembros*/

    printf( "%ld %s %d %f", Caballos.die, Caballos.nombre, Caballos.victorias, Caballos.ganancias );


}

void gestionMenuCaballos()
{

    /*Esta funcion llama a la funcion menuCaballos que le devuelve la opcion que ha elegido el usuario . Mientras que la opcion recibida sea distinta
    de las opciones validas , sigue llamando a menuCaballos. Si la opcion introducida es una de las opciones validas , utilizando un switch , llama a las funciones
    correspondientes : 1 = listado de caballos , 2 = informe de caballos , 3 = alta caballo , 4 = actualizaCaballos , 5 = bajaCaballos , 6 = caballo mas rentable
     , 7 = caballos mas victorioso , 8 = clasificaCaballos , 9 = fusionCaballos , a = fusionListaCaballos y 0= vuelve al menu princiapal */


    char mensaje [50] = "Error";
    int opcionValida = 10;

    while ((opcionValida!=1)&&(opcionValida!=2)&&(opcionValida!=4)&&(opcionValida!=3)&&(opcionValida !=0 ) && (opcionValida !=5)&&(opcionValida!=6)&&(opcionValida!=1)&&(opcionValida!=7)&&(opcionValida!=8)&&(opcionValida!=9) && (opcionValida !=49))
    {

        int opcionValida = menuCaballos();

        switch (opcionValida )
        {

        case 1 :
            limpiar();
            listadoCaballos( "caballos.dat");
            pararPrograma();
            break;

        case 2 :

            limpiar();
            generaInformeCaballos( mensaje);
            system( "notepad BaseDatos/infoCaballos.txt" );
            pararPrograma();

            break;

        case 3 :

            limpiar();
            altaCaballoIU();
            pararPrograma();

            break;

        case 4 :

            limpiar();
            actualizaCaballoIU();
            pararPrograma();
            break;

        case 5 :

            limpiar();
            bajaCaballoIU();
            pararPrograma();

            break;

        case 6 :

            limpiar();
            caballoMasRentableIU();
            pararPrograma();

            break;

        case 7 :

            limpiar();
            caballoMasVictoriosoIU();
            pararPrograma();

            break;

        case 8 :

            limpiar();
            clasificaCaballos();
            pararPrograma();
            break;

        case 9 :

            limpiar();
            fusionaCaballos();
            pararPrograma();
            break;

        case 49 :

            limpiar();
            fusionaListaCaballos();
            pararPrograma();
            break;


        case 0 :

            limpiar();
            gestionMenuPrincipal();

            break;
        }
    }

}

void listadoCaballos( char nombreBinario[50])
{

    /*Recibe como parametro el nombre del fichero donde est�n los caballos a mostrar . Esta funcion declara una estructura de tipo sCaballo . Llama a cargaListaCaballosSYs , enviandole la direccion de la estructura
    y enviando tambi�n el nombre del fichero , para que cargue los datos de los caballos.A traves de un bucle va mostrando los datos de los caballos  , a atraves de la funcion muestraCaballos.*/

    int  i, n;

    struct sListaCaballos Caballos;

    n = cargaListaCaballosSys( &Caballos, "error", nombreBinario);

    if ( n == 1)
    {

        gotoxy(10, 6);
        printf( "Listado de caballos ");
        gotoxy(1, 9);
        printf(" DIE      NOMBRE          VICTORIAS         GANANCIAS    ");

        for ( i=0 ; i<=(Caballos.numero_caballos-1) ; i++ )
        {

            gotoxy( 2, 13 +i );
            muestraCaballo( Caballos.Caballos[i] );

        }

    }
    else
    {

        muestraMensajeInfo( " Error obteniendo datos de los caballos " );

    }

}


int menuCaballos(void)
{

    // Muestra las distintas opciones que hay en menuPrincipal y llama a leerOpcionValida para obtener la opcion del usuario y la devuelve

    int opcionValida;

    gotoxy( 20, 6 );
    printf(" GESTION DE CABALLOS " );

    gotoxy( 2, 13);
    printf( " 1.Listado de caballos " );

    gotoxy( 2, 14);
    printf( " 2.Informe de caballos " );

    gotoxy( 2, 15);
    printf( " 3.Alta de un caballo " );

    gotoxy( 2, 16);
    printf( " 4.Actualizar un caballo " );

    gotoxy( 2, 17);
    printf( " 5.Baja de un caballo " );

    gotoxy( 2, 18);
    printf( " 6.Caballo mas rentable " );

    gotoxy( 2, 19);
    printf( " 7.Caballo con mas victorias " );

    gotoxy( 2, 20);
    printf( " 8.Clasificacion de caballos " );

    gotoxy( 2, 21);
    printf( " 9.Fusion de caballos " );

    gotoxy( 2, 22);
    printf( " a.Fusiona lista  de caballos " );


    gotoxy( 2, 24);
    printf( " 0.M e n u   a n t e r i o r " );

    opcionValida = leerOpcionValida( " Seleccione una opcion: ", 5);

    return opcionValida;


}


void bajaCaballoIU()
{

    /* Esta funcion declara dos listas de caballos distintas . En Caballos almacena los datos de las caballos , que son cargados a traves de
    la funcion cargaListaCaballosSys . Pide el DIE del caballo que quiere dar de baja . A traves de un bucle y un if , va comprobando si alguno de los
    DIEs de los caballos coincide con el introducido . Si alguno coincide , copia en la otra lista de Caballos ( CaballosF) los datos de los caballos
    originales , pero se salta el caballo que ha coincidido . Define el numero de caballos de CaballosF como uno menos que el de Caballos. Por ultimo ,
    llama a guardaListaCaballosSys y le envia CaballosF( la lista de caballos sin el caballo dado de baja) ,para que escriba en caballos.dat la nueva lista*/

    int  i, n;
    int m = 0;
    long int dieB;
    struct sListaCaballos CaballosF;
    int l =0;



    struct sListaCaballos Caballos;


    listadoCaballos( "caballos.dat");
    muestraMensajeInfo( "Introduzca el DIE del caballo a dar de baja: ");
    scanf( "%ld", &dieB);
    n = cargaListaCaballosSys( &Caballos, "Error", "caballos.dat" );
    int numero = Caballos.numero_caballos;

    CaballosF.Caballos = malloc( numero*sizeof(struct sCaballo));

    if ( n == -1)
    {

        muestraMensajeInfo( "No se ha podido cargar la lista de caballos");
        return;

    }



    for ( i = 0 ; i<= (Caballos.numero_caballos)-1 ; i++ )
    {

        if ( Caballos.Caballos[i].die == dieB)
        {
            numero--;


            CaballosF.Caballos = realloc( CaballosF.Caballos , numero * sizeof( struct sCaballo) );

            for ( m = 0 ; m <= (Caballos.numero_caballos)-1 ; m++)
            {


                if ( Caballos.Caballos[m].die == dieB)
                {

                    m = m+1;


                }



                strcpy( CaballosF.Caballos[l].nombre, Caballos.Caballos[m].nombre);
                CaballosF.Caballos[l].die = Caballos.Caballos[m].die;
                CaballosF.Caballos[l].ganancias = Caballos.Caballos[m].ganancias;
                CaballosF.Caballos[l].victorias = Caballos.Caballos[m].victorias;

                l++;

            }


            CaballosF.numero_caballos = (Caballos.numero_caballos ) -1;
            guardaListaCaballosSys( CaballosF, "Error", "caballos.dat");
            limpiar();
            listadoCaballos( "caballos.dat");
            muestraMensajeInfo("Su caballo ha sido dado de baja con exito " );

            break;



        }
        else if ( i == (Caballos.numero_caballos)-1)
        {

            limpiarMensaje();
            muestraMensajeInfo( "El caballo seleccionado no esta registrado");
            return;


        }

    }
}

void actualizaCaballoIU()
{

    /*Esta funcion declara una lista de caballos . Pide al usuario el caballo ganador y cuanto ha ganado . Almacena la informacion recibida en unas variables seguidas
    de V (nombreV y gananciasV . Llama a la funcion cargalistaSys para cargar los datos de los caballos en la lista declarada. Una vez recibidos los datos cargados
    correctamente , recorre todos los caballos , comparando el nombre de cada uno con el nombre introducido a partir de la funcion strcomp. Si se encuentra el caballo,
    se actualizan sus datos sumandole una victoria y las ganancias obtenidas , y se sale del bucle con un break. Si no se encuentra el caballo ( es decir , el bucle for
    llega al ultimo caballo sin haber encontrado nada , se sale de la funcion.*/

    listadoCaballos( "caballos.dat");

    char nombreV[20];
    float gananciasV;
    int  i, n;


    struct sListaCaballos Caballos;
    muestraMensajeInfo("Introduce el nombre del caballo ganador:" );
    scanf( "%s", nombreV );
    limpiarMensaje();

    muestraMensajeInfo( "Introduce cuanto ha ganado:" );
    scanf( " %f", &gananciasV );
    limpiarMensaje();

    n = cargaListaCaballosSys( &Caballos, "Error", "caballos.dat");

    if ( n == -1 )
    {

        muestraMensajeInfo("No se ha podido cargar la lista de caballos" );
        return;


    }
    else
    {

        for ( i = 0 ; i<=(Caballos.numero_caballos)-1 ; i++)
        {

            if ( strcmp( nombreV, Caballos.Caballos[i].nombre) == 0 )
            {

                Caballos.Caballos[i].victorias= Caballos.Caballos[i].victorias+1;
                Caballos.Caballos[i].ganancias = Caballos.Caballos[i].ganancias + gananciasV;

                guardaListaCaballosSys( Caballos, "Error", "caballos.dat");
                break;

            }
            else if ( i == (Caballos.numero_caballos)-1)
            {

                limpiarMensaje();
                muestraMensajeInfo("No hay ningun caballo registrado a ese nombre");
                return;

            }

        }
    }

    limpiar();
    listadoCaballos( "caballos.dat");
    muestraMensajeInfo("Su caballo ha sido actualizado con exito " );
}


void caballoMasRentableIU()
{

    /*Lo primero declara una lista de caballos y carga los datos de los caballos a traves de la funcion cargaListaCaballosSys. Si todo ha ido bien , recorre los distintos caballos
    , comparando sus ganancias con la variable gananciasM , que guarda las ganancias mas altas hasta el momento.Una vez recorridos todos los caballos , y habiendo guardado la coordenada
    del vector del caballo que ha superado por ultima vez gananciasM , muestra dicho caballo y termina el programa*/

    int n, i, l;
    float gananciasM = 0;

    gotoxy( 9, 7 );
    printf( "C A B A L L O    M A S    R E N T A B L E" );

    struct sListaCaballos Caballos;

    n = cargaListaCaballosSys( &Caballos, "Error", "caballos.dat");

    if ( n == -1)
    {

        muestraMensajeInfo( "Ha ocurrido un error abriendo el archivo ");
        return;

    }
    else
    {

        for ( i = 0 ; i<= (Caballos.numero_caballos)-1 ; i++)
        {

            if ( Caballos.Caballos[i].ganancias > gananciasM)
            {

                gananciasM = Caballos.Caballos[i].ganancias;
                l = i;

            }


        }


        gotoxy( 2, 12);
        muestraCaballo( Caballos.Caballos[l]);
        muestraMensajeInfo( "El caballo mas rentable es " );
        printf( "%s", Caballos.Caballos[l].nombre);


    }




}

void caballoMasVictoriosoIU()
{
    /*Esta funcion realiza exactamente lo mismo que la anterior pero como esta vez lo que interesa son las victorias , va comparando las victorias de cada caballo.
    El resto del procedimiento es exactamente igual que la funcion anterior*/

    int n, i, l;

    int victoriasM = 0;
    struct sListaCaballos Caballos;

    gotoxy( 9, 7 );
    printf( "C A B A L L O    M A S    V I C T O R I O S O" );

    n = cargaListaCaballosSys(  &Caballos, "Error", "caballos.dat");

    if ( n == -1)
    {

        muestraMensajeInfo( "Ha ocurrido un error abriendo el archivo ");
        return;

    }
    else
    {

        for ( i = 0 ; i<= (Caballos.numero_caballos)-1 ; i++)
        {

            if ( Caballos.Caballos[i].victorias > victoriasM)
            {

                victoriasM = Caballos.Caballos[i].victorias;
                l = i;

            }


        }


        gotoxy( 2, 12);
        muestraCaballo( Caballos.Caballos[l]);
        muestraMensajeInfo( "El caballo mas victorioso es " );
        printf( "%s", Caballos.Caballos[l].nombre );

    }

}

void clasificaCaballos()
{

    /*Lo primero que hace esta funci�n es cargar dos listas de caballos din�micas. Llama a listadoCabllos para que muestre los caballos a clasificar.Pide al usuario que introduzca
    el numero minimo de victorias para separar a los caballos y el nombre del archivo donde quiere almacenar los caballos que cumplan el requisito de victorias. Una vez hecho eso ,
    llama a cargaListaCaballosSys para que cargue los datos de los caballos y va recorriendo todos los caballos comprobando si cumplen o no la condicion de victorias minimas.
    Cuando un caballo cumple la condicion , copia sus datos en la otra lista de caballos dinamica , teniendo que actualizar con la funcion realloc el numero de vectores de las listas
    din�micas. Le env�a la segunda lista a guardaListaCaballosSys y muestra con listadoCaballos dicha lista. Por �ltimo libera las listas din�micas.*/


    gotoxy(8, 6);
    printf( "C L A S I F I C A C I O N   D E  C A B A L L O S ");

    struct sListaCaballos Caballos;

    struct sListaCaballos Caballos2;

    char archivo[50];

    int n, i, numero = 0;

    int victoriasMin;

    listadoCaballos( "caballos.dat");

    Caballos2.Caballos = NULL;
    muestraMensajeInfo("Introduzca el numero de victorias para la primera division:" );
    scanf( "%d", &victoriasMin );
    muestraMensajeInfo("Introduzca el nombre del archivo donde quiera guardar el resultado:" );
    scanf( "%s", archivo );

    n = cargaListaCaballosSys( &Caballos, "Error", "caballos.dat" );

    if ( n == 1)
    {

        for ( i=0 ; i<Caballos.numero_caballos ; i++)
        {

            if ( Caballos.Caballos[i].victorias >= victoriasMin)
            {

                numero++;
                Caballos2.Caballos = realloc(  Caballos2.Caballos, numero*sizeof(struct sCaballo));
                Caballos2.Caballos[numero-1].die = Caballos.Caballos[i].die;
                Caballos2.Caballos[numero-1].victorias = Caballos.Caballos[i].victorias;
                Caballos2.Caballos[numero-1].ganancias = Caballos.Caballos[i].ganancias;
                strcpy( Caballos2.Caballos[numero-1].nombre,  Caballos.Caballos[i].nombre);

            }

        }

    }
    else
    {

        return;

    }

    Caballos2.numero_caballos = numero;

    char binario[50] = ".dat";
    strcat ( archivo, binario );


    guardaListaCaballosSys( Caballos2, "Error", archivo );
    limpiar();

    listadoCaballos( archivo );
    gotoxy(8, 6);
    printf( "C L A S I F I C A C I O N   DE  C A B A L L O S ");

    limpiarMensaje();
    muestraMensajeInfo("Archivo creadao con exito");

    free( Caballos.Caballos);
    free( Caballos2.Caballos);

}

void muestraListaCaballos( struct sListaCaballos Caballos)
{

    /*Est� funci�n hace lo mismo que listadoCaballos pero recibiendo una lista din�mica como parametro , no un fichero.Utiliza un bucle for para ir mostrando los caballos de la
    lista*/

    gotoxy(1, 9);
    printf(" DIE       NOMBRE          VICTORIAS         GANANCIAS    ");

    int i ;

    for ( i=0 ; i<Caballos.numero_caballos ; i++ )
    {

        gotoxy( 2, 13 +i );
        muestraCaballo( Caballos.Caballos[i] );

    }
}

void fusionaCaballos()
{

    /*Esta funcion declara dos listas de caballos din�micas y carga los caballos de cabllos1.dat y caballos2.dat en dichas listas. Con un par de bucles for , va comprobando para cada
    caballo de la segunda lista si hay alg�n caballo en la primera que se llama igual . Cuando dos caballos coinciden , se suman sus victorias y ganancias y se guarda en la lista 1 . Cuando
    un caballo est� en la lista 2 pero no en la 1 , se guardan sus datos la final de la primera lista. Una vez comprobados y repartidos todos los caballos , los guarda en caballos3.dat a trav�s
    de la funci�n guardaListaCaballosSysy la muestra con la funci�n muestraListaCaballos. . Por �ltimo libera las listas din�micas*/

    gotoxy(8, 6);
    printf( "F U S I O N    D E    C A B A L L O S ");


    struct sListaCaballos Caballos1;
    struct sListaCaballos Caballos2;
    int o ;

    char lista1[50] = "caballos1.dat";
    char lista2[50] = "caballos2.dat";

    cargaListaCaballosSys( &Caballos1, "Error abriendo caballos1.dat ", lista1);
    cargaListaCaballosSys( &Caballos2, "Error abriendo caballos2.dat ", lista2);



    int n, m =0;

    for( m = 0 ; m<Caballos2.numero_caballos ; m++)
    {
        o = Caballos1.numero_caballos;


        for( n = 0 ; n<o ; n++)
        {

            if( strcmp( Caballos1.Caballos[n].nombre, Caballos2.Caballos[m].nombre) == 0)
            {


                Caballos1.Caballos[n].victorias = Caballos2.Caballos[m].victorias + Caballos1.Caballos[n].victorias;
                Caballos1.Caballos[n].ganancias = Caballos2.Caballos[m].ganancias + Caballos1.Caballos[n].ganancias;
                break;


            }
            else if( (n == (Caballos1.numero_caballos) - 1)&&(strcmp(Caballos2.Caballos[m].nombre, Caballos1.Caballos[n].nombre ) != 0))
            {


                Caballos1.numero_caballos++;

                Caballos1.Caballos = realloc(  Caballos1.Caballos, Caballos1.numero_caballos*sizeof(struct sCaballo));
                strcpy( Caballos1.Caballos[Caballos1.numero_caballos-1].nombre, Caballos2.Caballos[m].nombre);
                Caballos1.Caballos[Caballos1.numero_caballos-1].die = Caballos2.Caballos[m].die;
                Caballos1.Caballos[Caballos1.numero_caballos-1].victorias = Caballos2.Caballos[m].victorias;
                Caballos1.Caballos[Caballos1.numero_caballos-1].ganancias = Caballos2.Caballos[m].ganancias;

            }
        }

    }


    guardaListaCaballosSys( Caballos1,"Error ", "caballos3.dat");

    muestraListaCaballos( Caballos1 );


    gotoxy(21, 30 );
    printf( "Archivo %s y archivo %s fusionados en caballos3.dat", lista1, lista2);

    free( Caballos1.Caballos);
    free(Caballos2.Caballos);


}

void fusionaListaCaballos()
{

    /*Esta funci�n es similar a la anterior . Declara tres listas din�micas de caballos y s�lo guarda en la tercera los caballos que estan tanto en la lista 1 como en la lista 2.
    Es decir , guarda en Caballos3 los caballos que est�n a la vez en caballos1.dat y caballos2.dat. Primero muestra ambas listas y luego a trav�s de dos bucles for y un if va
    comparando los DIEs de cada cabllo de cada lista y guardando en la tercera lista los de los caballos que coincidan. Guarda Caballos3 en el fichero caballos4.dat a trav�s de la
    funci�n guardaListaCaballosSys y la muestra con muestrListaCaballos . Por �ltimo libera las tres listas din�micas*/


    struct sListaCaballos Caballos1;
    struct sListaCaballos Caballos2;
    struct sListaCaballos Caballos3;

    int m, n;

    cargaListaCaballosSys( &Caballos1, "Error", "caballos1.dat");
    cargaListaCaballosSys(&Caballos2, "Error", "caballos2.dat");

    muestraListaCaballos(Caballos1);
    muestraMensajeInfo( "Archivo caballos1.dat");
    pararPrograma();


    muestraListaCaballos( Caballos2 );
    muestraMensajeInfo("Archivo caballos2.dat" );
    pararPrograma();

    Caballos3.Caballos = malloc( 1*sizeof(struct sCaballo) );
    Caballos3.numero_caballos = 0;

    for( m = 0 ; m< Caballos2.numero_caballos ; m++)
    {

        for( n = 0 ; n < Caballos1.numero_caballos ; n++)
        {


            if ( Caballos1.Caballos[n].die == Caballos2.Caballos[m].die )
            {


                Caballos3.numero_caballos++;
                Caballos3.Caballos = realloc( Caballos3.Caballos,Caballos3.numero_caballos * sizeof(struct sCaballo) );
                strcpy( Caballos3.Caballos[Caballos3.numero_caballos -1].nombre, Caballos1.Caballos[n].nombre);
                Caballos3.Caballos[Caballos3.numero_caballos -1].die = Caballos1.Caballos[n].die;
                Caballos3.Caballos[Caballos3.numero_caballos -1].ganancias = Caballos1.Caballos[n].ganancias + Caballos2.Caballos[m].ganancias;
                Caballos3.Caballos[Caballos3.numero_caballos -1].victorias =  Caballos1.Caballos[n].victorias + Caballos2.Caballos[m].victorias;

            }

        }

    }

    guardaListaCaballosSys( Caballos3, "Error", "caballos4.dat");

    muestraListaCaballos( Caballos3 );

    gotoxy(8, 6);
    printf( "F U S I O N    D E   L I S T A    C A B A L L O S ");

    muestraMensajeInfo( "Archivo caballo1.dat y caballo2.dat fusionados con exito en caballos4.dat." );

    free( Caballos1.Caballos);
    free(Caballos2.Caballos);
    free( Caballos3.Caballos);



}








