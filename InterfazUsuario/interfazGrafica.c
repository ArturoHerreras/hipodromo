

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "interfazGrafica.h"
#include "interfazUsuario.h"
#include <conio.h>
#include "../Entidades/caballo.h"
void muestraMensajeInfo( char *msg )
{

    // Muestra el mensaje que se introduce como parametro en una parte de la pantalla

    gotoxy(21, 30 );
    printf( "%s", msg );

}
void setColorTexto(WORD colors)
{
    HANDLE hConsole=GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hConsole, colors);
}
void gotoxy(int x, int y)
{
    COORD coord;

    coord.X = x;
    coord.Y = y;

    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);

    return;
}

int leerOpcionValida( char *mensaje, int num )
{

    // Pide al usuario que introduce la opcion que quieras y la devuelve , siempre y cuando este en el rango de valores admitido

    int opcion = 1;

    while ( (0<= opcion)&&(opcion  <= num) )
    {

        muestraMensajeInfo( mensaje );

        opcion = getche() - 48 ;

        return opcion;

    }

    return opcion;

}




void limpiar()
{

    //Esta funcion limpia la pantalla menos el marco y el titulo

    int i, j;
    for( i = 1 ; i<= 60 ; i++  )
    {
        gotoxy( i, 6 );
        printf( " " );
    }
    for( i = 1 ; i<= 60 ; i++  )
    {
        gotoxy( i, 7 );
        printf( " " );
    }
    for( i = 1 ; i<= 60 ; i++  )
    {
        gotoxy( i, 9 );
        printf( " " );
    }
    for( i = 1 ; i<= 60 ; i++  )
    {
        gotoxy( i, 10 );
        printf( " " );
    }
    for( i = 1 ; i<= 60 ; i++  )
    {

        for( j =12 ; j<=27 ; j++)
        {
            gotoxy( i, j );
            printf( " " );
        }
    }
    for( i = 1 ; i<= 60 ; i++  )
    {
        gotoxy( 63+i, 6 );
        printf( " " );
    }
    for( i = 1 ; i<= 60 ; i++  )
    {
        gotoxy( 63+i, 7 );
        printf( " " );
    }
    for( i = 1 ; i<= 60 ; i++  )
    {
        gotoxy( 63+i, 9 );
        printf( " " );
    }
    for( i = 1 ; i<= 60 ; i++  )
    {
        gotoxy( 63+i, 10 );
        printf( " " );
    }
    for( i = 1 ; i<= 60 ; i++  )
    {

        for( j =12 ; j<=27 ; j++)
        {
            gotoxy( 63+i, j );
            printf( " " );
        }
    }

    for( i = 1 ; i<=82 ; i++)
    {
        gotoxy( 20+i, 30);
        printf( " " );
    }

    for( i = 1 ; i<=82 ; i++)
    {
        gotoxy( 20+i, 32);
        printf( " " );
    }

}

void limpiarMensaje()
{

    // Esta funcion limpia la zona de mensaje


    int i ;

    for( i = 1 ; i<=82 ; i++)
    {
        gotoxy( 20+i, 30);
        printf( " " );
    }

    for( i = 1 ; i<=82 ; i++)
    {
        gotoxy( 20+i, 32);
        printf( " " );
    }

}










